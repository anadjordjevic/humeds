#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


int new_read_sig(char * file_name, char * path,  long int sig_len)
//int new_read_sig(char * file_name, char * path,  long int sig_len, float * M[])
{	
	/*variables*/
	FILE * f1, * f2;
	char * header_file_name, * dat_file_name, * signalh, * signald, * z;
	unsigned char * buffer;
	int bitres, dformat, gain, leadsNo, fs;
	long zerovalue,  firstvalue, file_size, size, samples2read;
	long * M2H, * M1H,  * PRL, * PRR;
	long M1, M2, M3, r, c, i, j, k;
	long ** A, ** m;
	float ** M;

	/*allocate header_file_name and dat_file_name*/
	header_file_name = (char *) malloc(1000 * sizeof(char));
	dat_file_name = (char *) malloc(1000 * sizeof(char));
	
	/*copy file_name variable to  header_file_name and dat_file_name*/
	memcpy(header_file_name, file_name, (100*sizeof(header_file_name)));
	memcpy(dat_file_name, file_name, (100*sizeof(dat_file_name)));
	
	
// ------ LOAD HEADER DATA --------------------------------------------------
	/*allocate signalh, z*/
	signalh = (char *) malloc(1000 * sizeof(char));
	z = (char*) malloc(1000 * sizeof(char));

	/*open .hea file*/
	header_file_name = strcat(header_file_name, ".hea"); //header-file in text format
    strcpy(signalh, path); 
	signalh = strcat(signalh, header_file_name); 
	f1 = fopen(signalh,"r");           // open file for reading
	/*reading 1st row in .hea*/
	fgets(z, 1000, f1);
	sscanf(z, "%*s %d %d", &leadsNo, &fs);
	/*reading 2nd row in .hea*/
	fgets(z, 1000, f1);
	sscanf(z, "%*s %d %d %d %ld %ld", &dformat, &gain, &bitres, &zerovalue, &firstvalue);
	fclose(f1);
	free(z);

//------ LOAD BINARY DATA --------------------------------------------------
	/*allocate signald*/
	signald = (char *) malloc(1000 * sizeof(char));

	/*open .dat file*/ 
	dat_file_name = strcat(dat_file_name, ".dat");   //data-file
    strcpy(signald, path); 
	signald= strcat(signald, dat_file_name);   
	f2=fopen(signald,"rb");
	fseek(f2, 0, SEEK_END); 
	file_size = ftell(f2);

	/*check format and find size of signal*/
	if (dformat == 212)
	{
		size = ((file_size*8) / 12);
		samples2read = size/leadsNo;
		/*set size of A - matrix with 3 columns, each 8 bits long = 2*12bit */
		r = samples2read;
		c = 3;  	
	}

	else if (dformat == 318)
	{
		size = ((file_size*8) / 18);
		samples2read = file_size/7;
		/*set size of A - matrix with 7 columns, each 8 bits long = 3*18bit */
		r = samples2read;
		c = 7; 
	}
	fseek(f2, 0, SEEK_SET); 

	/*allocate buffer, A*/
	buffer = (unsigned char *) malloc(file_size * sizeof(unsigned char));
	//correct allocation?!
	A = (long **) malloc(r * sizeof(long));
	for(i = 0; i < r; i++)
			A[i] = (long *) malloc(c * sizeof(long));
	

	/*reading binfile in buffer*/
	fread(buffer,1, file_size,f2); 
	fclose(f2);

	/*reading from buffer*/
	k=0;
	while (k <(r*c))
	{
		for(i = 0; i<r; i++)
		{
			for(j = 0; j<c; j++)
			{
				A[i][j] = buffer[k];
				k++;
			}
		}
	}	

	free(buffer);
	free(header_file_name);
	free(dat_file_name);
	free(signalh);
	free(signald);

	/*allocate M = leadsNo*samples2read */
	//correct allocation!
	c = leadsNo;
	//for(i=0;i<leadsNo;i++)
		//M[i] = (float *) malloc(r * sizeof(float));
	M = (float **) malloc(c * sizeof(float));;
	for(i=0;i<c;i++)
		M[i] = (float *) malloc(r * sizeof(float));

	if (leadsNo == 2)
	{
		/*allocate M2H, M1H, PRL, PRR*/
		M2H = (long *) malloc(r * sizeof(long));
		M1H = (long *) malloc(r * sizeof(long));
		PRL = (long *) malloc(r * sizeof(long));
		PRR = (long *) malloc(r * sizeof(long));

		/*reading signal*/
		for (i = 0; i < r; i++) 
		{
			M2H[i] = A[i][1] >> 4; // divide by 2^-4
			M1H[i] = A[i][1] & 15;
			PRL[i] = (A[i][1] & 8) << 9; //sign-bit
			PRR[i] = (A[i][1] & 128) << 5; //sign-bit
			M[0][i] = (M1H[i] << 8) + (A[i][0] - PRL[i]);
			M[1][i] = (M2H[i] << 8) + (A[i][2] - PRR[i]);
			M[0][i] = (M[0][i] - zerovalue) / gain;
			M[1][i] = (M[1][i] - zerovalue) / gain;
				//printf("%f %f \n", M[0][i], M[1][i]);
		}
		free(M2H);
		free(M1H);
		free(PRL);
		free(PRR);
	}
	else if (leadsNo == 3)
	{
		/*allocate m and set value to M1, M2, M3*/
		//correct allocation?!
		m = (long **) malloc(c * sizeof(long));
		for(i = 0; i < c; i++)
			m[i] = (long *) malloc(r * sizeof(long));
		M1 = -1;
		M2 = -1;
		M3 = -1;

		/*reading signal*/
		for (i = 0; i < r; i++) 
		{
			/*lead 1*/
			m[0][i] = m[0][i] & 0;
			m[0][i] = (m[0][i] + (A[i][2] & 3)) << 8;
			m[0][i] = (m[0][i] + A[i][1]) << 8;
			m[0][i] = m[0][i] + A[i][0];
			if (((A[i][2] & 2) > 0) && (M1 == -1))
			{
				M1 = m[0][i];
			}
			if (i == M1)
			{
				m[0][i] = m[0][i] | 4294705152;
			}
					

			/*lead 2*/
			m[1][i] = m[1][i] & 0;
			m[1][i] = (m[1][i] + (A[i][4] & 15)) << 8;
			m[1][i] = (m[1][i] + A[i][3]) << 6;
			m[1][i] = m[1][i] + (((A[i][2] & 252) >> 2) & 63);
			if (((A[i][4] & 8) > 0) && (M2 == -1))
			{
				M2 = m[1][i];
			}
			if (i == M2)
			{
				m[1][i] = m[1][i] | 4294705152;
			}

			
			/*lead 3*/
			m[2][i] = m[2][i] & 0;
			m[2][i] = (m[2][i] + (A[i][6] & 63)) << 8;
			m[2][i] = (m[2][i] + A[i][5]) << 4;
			m[2][i] = m[2][i] + (((A[i][4] & 240) >> 4) & 15);
			if (((A[i][6] & 32) > 0) && (M3 == -1))
			{
				M3 = m[2][i];
			}
			if (i == M3)
			{
				m[2][i] = m[2][i] | 4294705152;
			}
					
			/*M = [lead1, lead2, lead3]*/
			if (m[0][i] <= 131072) // (2^7)*1024 = 131072
				M[0][i] = m[0][i];
			else
				M[0][i] = m[0][i] - 262144; //(2^8)*1024 = 262144
					
			if (m[1][i] <= 131072) 
				M[1][i] = m[1][i];
			else
				M[1][i] = m[1][i] - 262144; 

			if (m[2][i] <= 131072) 
				M[2][i] = m[2][i];
			else
				M[2][i] = m[2][i] - 262144; 
					
			printf("%f %f %f \n", M[0][i], M[1][i], M[2][i]);
		}
		free(m);
	}
	free(A);
	
	sig_len = size;
	return(0);

}