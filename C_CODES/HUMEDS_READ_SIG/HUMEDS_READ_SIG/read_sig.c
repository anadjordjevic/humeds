#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

double ** read_sig(char * num, char * path, double p1, double p2, int arr_plot_flag)
{	
	
	FILE * f1, * f2;
	int i, j, k, l;
	char * HEADERFILE, * DATAFILE;
	char * num1, * num2, * path1, * path2;
	char * signalh, * signald;
	char * z;
	unsigned char * buffer;
	int * dformat;
	long * gain, * bitres, * zerovalue, * firstvalue;
	long * M2H, * M1H,  * PRL, * PRR; 
	long SAMPLES2READ;
	long r,c;
	long nosig, sfreq;
	long size;
	long ** A;
	double **M;
	//double **M1;	
	         
	HEADERFILE = (char *) malloc(10000 * sizeof(char *));
	DATAFILE = (char *) malloc(1000 * sizeof(char *));
	SAMPLES2READ = (long) malloc(650000 * sizeof(long));
	num1 = (char *) malloc(100 * sizeof(char *));
	num2 = (char *) malloc(100 * sizeof(char *));
	path1 = (char *) malloc(100 * sizeof(char *));
	path2 = (char *) malloc(100 * sizeof(char *));
	
		
	/*copy of num variable */
	memcpy(num1, num, 100*sizeof(char));
	memcpy(num2, num, 100*sizeof(char));
	
	/*copy of path variable */
	memcpy(path1, path, 100*sizeof(char));
	memcpy(path2, path, 100*sizeof(char));
	

	HEADERFILE = strcat(num1, ".hea"); //header-file in text format
	DATAFILE = strcat(num2, ".dat");   //data-file
	SAMPLES2READ = 650000;			  //number of samples to be read


// ------ LOAD HEADER DATA --------------------------------------------------
	/*allocate signalh, z, A*/
	signalh = (char *) malloc(1000 * sizeof(char *));
	z = (char*) malloc(1000 * sizeof(char *));
	r = 1; 
	c = 2;
	A = (long **) malloc(r * sizeof(long *));
	for(i = 0; i < r; i++)
		A[i] = (long *) malloc(c * sizeof(long *));

	
	signalh = strcat(path1, HEADERFILE); // build fullfile name from the inputs
	f1 = fopen(signalh,"r");           // open file for reading
	fgets(z, 1000, f1);
	sscanf(z, "%*s %d %d ", &A[0][0], &A[0][1]);
	nosig = A[0][0];		//number of signals
	sfreq = A[0][1];		// sample rate of data
	

//-----------------------------------------------------------------------------------
	/* allocate dformat, gain, bitres, zerovalue, firstvalue*/
	dformat = (int *) malloc(nosig * sizeof(int *));           
	gain = (long *) malloc(nosig * sizeof(long *));              
	bitres = (long *) malloc(nosig * sizeof(long *));            
	zerovalue = (long *) malloc(nosig * sizeof(long *));        
	firstvalue = (long *) malloc(nosig * sizeof(long *));

	/*new size of A*/
	r = 1;
	c = 5;
		
	for (i=0; i<nosig; i++)
	{
		fgets(z, 1000, f1);
		sscanf(z, "%*s %d %d %d %d %d", &A[0][0], &A[0][1], &A[0][2], &A[0][3], &A[0][4]);
		dformat[i]= A[0][0];           //format; here only 212 is allowed
		gain[i]= A[0][1];              // number of integers per mV
		bitres[i]= A[0][2];            // bitresolution
		zerovalue[i]= A[0][3];         // integer value of ECG zero point
		firstvalue[i]= A[0][4];        // first integer value of signal (to test for errors)*/
	}

	fclose(f1);

//------ LOAD BINARY DATA --------------------------------------------------
	/*allocate signald*/
	signald = (char *) malloc(1000 * sizeof(char *));
	signald= strcat(path2, DATAFILE);   // data in format 212
	f2=fopen(signald,"rb");
	fseek(f2, 0, SEEK_END); 
	size = ftell(f2); 
	fseek(f2, 0, SEEK_SET); 

	/*allocate buffer, new A*/
	buffer = (unsigned char *) malloc(size * sizeof(unsigned char *));
	r = SAMPLES2READ;
	c = 3;
	A = (long **) malloc(r * sizeof(long *));
	for(i = 0; i < r; i++)
		A[i] = (long *) malloc(c * sizeof(long *));


	/*reading binfile in buffer*/
	fread(buffer,1, size,f2); 
	fclose(f2);
	/*reading from buffer*/
	k=0;
	while (k<size)
	{
		for(i = 0; i<r; i++)
		{
			for(j = 0; j<c; j++)
			{
				A[i][j] = buffer[k];
				k++;
			}
		}
	}	
	
	/*allocate M2H, M1H, PRL, PRR, M*/
	M2H = (long*) malloc(r * sizeof(long *));
	M1H = (long *) malloc(r * sizeof(long *));
	PRL = (long *) malloc(r * sizeof(long *));
	PRR = (long *) malloc(r * sizeof(long *));
	c=2;
	M = (double **) malloc(r * sizeof(double *));
	for(i = 0; i < r; i++)
		M[i] = (double *) malloc(c * sizeof(double *));

	for (i = 0; i < r; i++) 
	{
		M2H[i] = A[i][1] >> 4; // divide by 2^-4
		M1H[i] = A[i][1] & 15;
		PRL[i] = (A[i][1] & 8) << 9; //sign-bit
		PRR[i] = (A[i][1] & 128) << 5; //sign-bit
		M[i][0] = (M1H[i] << 8) + (A[i][0] - PRL[i]);
		M[i][1] = (M2H[i] << 8) + (A[i][2] - PRR[i]);
	}

	switch (nosig)
	{
		case 2 :
			{
			 for (i=0; i<r; i++){
				M[i][0] = (M[i][0] - zerovalue[0]) / gain[0];
				M[i][1] = (M[i][1] - zerovalue[1]) / gain[1];
				printf("%f %f \n", M[i][0], M[i][1] );
			 }
			}
		
		/*case 1 : 
			{
			M1 = (double *) malloc((r*c)-1 * sizeof(double));
			for (i=0; i<r; i++)
			{
				M[i][0] = (M[i][0] - zerovalue[0])/ gain[0];;
				M[i][1] = (M[i][1] - zerovalue[1])/ gain[0];;

				//delete first element and make M[]
				j = 0;
				k = 0,
				l = 1;
				while ((j < ((r*c)-1)) && (k < r) && (l < r))
				{
					if (j % 2 == 0) //j = even
					{ 
						M1[j] = M[i][1];
						k++;
					}
					else //j = odd
					{ 
						M1[j] = M[i][0];
						l++;
					}	
					j++;
				}	
			}
			}*/

	}
	
	return M;

}