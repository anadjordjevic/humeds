#include "humeds_bib.h"


/**---------------------------------------------------------------------------------------------
HERMITE_ENCODING:
Description:
   function encodes Lead1 and Lead2, results are saved in .txt file
Inputs: R_peaks_indx the index of R beats in the signal
        Lead1, Lead2 leads used for the encoding
        N number of hermite functions needed for the decomposition
        lim1 the low limit of sigma
        lim2 the high limit of sigma
        step_size is the step used for stepwise increment of sigma
        Leads_flag tells us about the used leads for clustering
        length in length od R_peaks_indx
        Sigma_NO is number of sigma
        zero_padding_len is zero padding size on each side
        QRS_len is QRS size on each side
        vector_len is length of vector, used in hermite function
        hermites is an array of all hermites needed
Output: Hermite_vector[][] saved in .txt file


Example: hermite_encoding(R_peaks_indx,Leads[0],Leads[1],5,18,51,0.5,[1,1], QRS_NO,sigma_NO, zero_padding_len, QRS_len, vector_len,hermites, PATH, File_name);

-------------------------------------------------------------------------------------------------**/

int hermite_encoding(int * R_peaks_indx, float * Lead1, float * Lead2,int N,float lim1,float lim2,
                     float step_size,int * Leads_flag, long int length, int sigma_NO,int zero_padding_len,
                     int QRS_len, int vector_len, float ***hermites, char *PATH, char * File_name)
{

    /*Read the R indexes*/
    int * R_points = (int*) malloc (length * sizeof(int));
    memcpy(R_points,R_peaks_indx,length * sizeof(int));
    long int Beats_NO = length;

    /*variables*/
    int sigma=sigma_NO;
    int i, j, k, l, m;

/*---------------------------heart rate information-------------------------------------------*/

    /*allocate and set RR*/
    float * RR =  (float*) malloc (length * sizeof(float));
    rr(R_points,length,RR);

     /*allocate R_plus, R_min, R_mult*/
    int B_NO = Beats_NO-3;
    float * R_plus= (float*)malloc (B_NO* sizeof(float));
    float * R_min= (float*) malloc (B_NO* sizeof(float));
    float * R_mult= (float*) malloc (B_NO* sizeof(float));

    /*clustered_R_points*/
    int * clustered_R_points = (int*) malloc (B_NO * sizeof(int));
    for (i=2;i<length-1;i++){
        clustered_R_points[i-2]=R_points[i];
    }
/*----------set Beats_L1, Beats_L1, R_plus, R_min, R_mult------------------------------------------------------------*/

    /*allocate Beats_L1, Beats_L2*/
    float * Beats_L1[vector_len];
    float * Beats_L2[vector_len];
    for (i=0; i < vector_len; i++)
    {
        Beats_L1[i] = (float*)malloc (B_NO* sizeof(float));
        Beats_L2[i] = (float*)malloc (B_NO* sizeof(float));
    }

    /*if Leads_flag(0)+Leads_flag(1)==2*/
    int Q = QRS_len/2;
    int Z = (zero_padding_len+QRS_len);
    if ((Leads_flag[0] + Leads_flag[1]) == 2)
    {
         /* set Beats_L1, Beats_L2, R_plus, R_min, R_mult */
        for (i=2; i < Beats_NO-1; i++)
        {
            k=R_points[i]-Q;
            for (j=zero_padding_len-1; j < Z; j++)
            {
                Beats_L1[j][i-2] = Lead1[k];
                Beats_L2[j][i-2] = Lead2[k];
                k++;
            }

            if (((RR[i]-RR[i-1])-(RR[i-1]-RR[i-2]))>0)
                R_plus[i-2] = (RR[i]-RR[i-1])-(RR[i-1]-RR[i-2]);
            else
                R_plus[i-2] = 0;
            R_min[i-2]=RR[i]-RR[i-1];
            R_mult[i-2]=RR[i+1]/RR[i];
        }
    }

    /*if Leads_flag(0))==1*/
    else if (Leads_flag[0] == 1)
    {
        /* set Beats_L1, R_plus, R_min, R_mult */
        for (i=2; i < Beats_NO-1; i++)
        {
            k=R_points[i]-Q;
            for (j=zero_padding_len-1; j < Z; j++)
            {
                Beats_L1[j][i-2] = Lead1[k];
                k++;
            }
          if (((RR[i]-RR[i-1])-(RR[i-1]-RR[i-2]))>0)
                R_plus[i-2] = 1 *((RR[i]-RR[i-1])-(RR[i-1]-RR[i-2]));
          else
                R_plus[i-2] = 0 *((RR[i]-RR[i-1])-(RR[i-1]-RR[i-2]));
          R_min[i-2]=RR[i]-RR[i-1];
          R_mult[i-2]=RR[i+1]/RR[i];
        }
    }

    /*if Leads_flag(1))==1*/
    else if (Leads_flag[1] == 1)
    {
        /* set Beats_L2 */
        for (i=2; i < Beats_NO-1; i++)
        {
            k=R_points[i]-Q;
            for (j=zero_padding_len-1; j < Z; j++)
            {
                Beats_L2[j][i-2] = Lead2[k];
                k++;
            }

            if (((RR[i]-RR[i-1])-(RR[i-1]-RR[i-2]))>0)
                R_plus[i-2] = 1 *((RR[i]-RR[i-1])-(RR[i-1]-RR[i-2]));
            else
                R_plus[i-2] = 0 *((RR[i]-RR[i-1])-(RR[i-1]-RR[i-2]));
            R_min[i-2]=RR[i]-RR[i-1];
            R_mult[i-2]=RR[i+1]/RR[i];
        }
    }

    /*free RR, R_points*/
    free(RR);
    free(R_points);

/*------------------------------Encoding process-------------------------------*/

    /*allocate Hermite_vector*/
    float * Hermite_vector[(2*(N+1))+3];
    for (j = 0; j<((2*(N+1))+3); j++ )
    {
        Hermite_vector[j] = ( float *)malloc (B_NO * sizeof( float));
    }


 /*-------------------------allocating arrays needed for encoding--------------------*/
    /*allocate x*/
    float * x =  malloc (vector_len* sizeof(float));

     /*allocate c, A*/
    float * c[N],* A[N];
    for (i=0; i < N; i++)
    {
        c[i] = ( float *)malloc (sigma_NO * sizeof( float));
        A[i] = ( float *)malloc (vector_len * sizeof( float));
    }

     /*allocate cn_phi*/
    float * cn_phi =  malloc (vector_len* sizeof(float));

    /*allocate et*/
    float * et[sigma_NO];
    int t=(vector_len-zero_padding_len*2);
    for (i=0; i < sigma_NO; i++)
    {
        et[i] = ( float *)malloc (t* sizeof( float));
    }

    /*allocate error_sigma*/
    float * error_sigma = ( float *)malloc (sigma_NO * sizeof( float));

/*-------------------------------------encoding for Lead1-----------------------------------------*/
    float isig;
    int sigmal;
    float tmp, sum;
    if (Leads_flag[0]==1)
    {
        /*allocate Lead_1_vector*/
        float * Lead_1_vector[N+1];
        for (i=0; i < N+1; i++)
        {
            Lead_1_vector[i] = malloc (B_NO * sizeof(float));
        }

        /*Encoding loop for Lead1 */
        for (i=0; i < B_NO; i++)
        {
            /*set x*/
            for (j=0; j < vector_len; j++)
            {
                 x[j] = Beats_L1[j][i];
            }

            /*loop for each sigmal*/
            k=-1;
            isig=lim1;
            while( isig <= (lim2-step_size))
            {
                k++;
                sigmal = ((isig-lim1)/step_size);

                /*set A*/
                for(j=0;j<N;j++)
                {
                    for(l=0;l<vector_len;l++)
                    {
                        A[j][l] = hermites[sigmal][j][l];
                    }
                }

                /*finding the coffecients corresponding to hermites functions*/
                /*set c*/
                for(l=0;l<N;l++)
                {
                    tmp = 0;
                    for(m=zero_padding_len-1;m<Z;m++)
                    {
                        tmp = tmp + A[l][m] * x[m];
                    }
                    c[l][k]=tmp;
                }

                /*set cn_phi*/
                switch(N)
                {
                    case 3 :
                      for(j=0;j<vector_len;j++)
                      {
                        cn_phi[j]=c[0][k]*A[0][j]+c[1][k]*A[1][j]+c[2][k]*A[2][j];
                      }
                      break;
                   case 4 :
                      for(j=0;j<vector_len;j++)
                      {
                        cn_phi[j]=c[0][k]*A[0][j]+c[1][k]*A[1][j]+c[2][k]*A[2][j]+c[3][k]*A[3][j];
                      }
                      break;
                   case 5 :
                      for(j=0;j<vector_len;j++)
                      {
                        cn_phi[j]=c[0][k]*A[0][j]+c[1][k]*A[1][j]+c[2][k]*A[2][j]+c[3][k]*A[3][j]+c[4][k]*A[4][j];
                      }
                      break;
                   case 6 :
                      for(j=0;j<vector_len;j++)
                      {
                        cn_phi[j]=c[0][k]*A[0][j]+c[1][k]*A[1][j]+c[2][k]*A[2][j]+c[3][k]*A[3][j]+c[4][k]*A[4][j]+c[5][k]*A[5][j];
                      }
                      break;
                }

                /*set et*/
                sum = 0;
                m=36;
                for (l= 0; l < t; l++)
                {
                    et[k][l] = pow((x[m] - cn_phi[m]),2);
                    m++;
                    sum += et[k][l];
                }
                /*set error_sigma*/
                error_sigma[k] = sum;

                isig += step_size;
            }
            /*finding min error_sigma*/
            float min = error_sigma[0];
            for ( j=0 ; j < sigma_NO ; j++ )
            {
                if ( error_sigma[j] <= min )
                {
                    min = error_sigma[j];
                    sigma = j;
                }
            }
            int m1 = sigma;
            /*new sigma*/
            sigma = ((sigma-1)*step_size+lim1);

            /*set Lead_1_vector*/
            for (j=0; j<N; j++)
            {
                Lead_1_vector[j][i] = c[j][m1];
            }
            Lead_1_vector[N][i]=sigma;

        }

// /*-------------------- beat vector, needed for plotting --------------------------*/
//            /*allocate beat, a1, a2, a3, a4, a5*/
//            float * beat = malloc (vector_len*sizeof(float));
//            float * a1 = malloc (vector_len*sizeof(float));
//            float * a2 = malloc (vector_len*sizeof(float));
//            float * a3 = malloc (vector_len*sizeof(float));
//            float * a4 = malloc (vector_len*sizeof(float));
//            float * a5 = malloc (vector_len*sizeof(float));
//            /*set a1, a2, a3, a4, a5 and beat*/
//            /*this also depend on N*/
//            a1 = Hermite_fun(vector,vector_len,sigma,0);
//            a2 = Hermite_fun(vector,vector_len,sigma,1);
//            a3 = Hermite_fun(vector,vector_len,sigma,2);
//            a4 = Hermite_fun(vector,vector_len,sigma,3);
//            a5 = Hermite_fun(vector,vector_len,sigma,4);
//            for (j = 0; j<vector_len; j++ )
//            {
//                beat[j]=c[0][m1]*a1[j]+c[1][m1]*a2[j]+c[2][m1]*a3[j]+c[3][m1]*a4[j]+c[4][m1]*a5[j];
//            }
//            /*free a1, a2, a3, a4, a5*/
//            free(a1);
//            free(a2);
//            free(a3);
//            free(a4);
//            free(a5);
//        }

        /*set Hermite vector[0:N+1]*/
        for (j=0; j<N+1; j++)
        {
            for (l=0; l<B_NO; l++)
            {
                Hermite_vector[j][l] = Lead_1_vector[j][l];
            }
        }

        for (i=0; i < N+1; i++)
        {
            free(Lead_1_vector[i]);
        }
        free(Lead_1_vector);
    }

/*--------------------------------encoding for Lead2---------------------------------------------------*/
    if (Leads_flag[1]==1)
    {
        /*allocate Lead_2_vector*/
        float * Lead_2_vector[N+1];
        for (i=0; i < N+1; i++)
        {
            Lead_2_vector[i] = malloc (B_NO * sizeof(float));
        }

        /*Encoding loop for Lead2 */
        for (i=0; i < B_NO; i++)
        {
            /*set x*/
            for (j=0; j < vector_len; j++)
            {
                 x[j] = Beats_L2[j][i];
            }
            /*loop for each sigmal*/
            k=-1;
            isig=lim1;
            while( isig <= (lim2-step_size))
            {
                k++;
                sigmal = ((isig-lim1)/step_size);

                /*set A*/
                for(j=0;j<N;j++)
                {
                    for(l=0;l<vector_len;l++)
                    {
                        A[j][l] = hermites[sigmal][j][l];
                    }
                }

                /*finding the coffecients corresponding to hermites functions*/
                /*set c*/
                for(l=0;l<N;l++)
                {
                    tmp = 0;
                    for(m=zero_padding_len-1;m<Z;m++)
                    {
                        tmp = tmp + A[l][m] * x[m];
                    }
                    c[l][k]=tmp;
                }

                 /*set cn_phi*/
                switch(N)
                {
                    case 3 :
                      for(j=0;j<vector_len;j++)
                      {
                        cn_phi[j]=c[0][k]*A[0][j]+c[1][k]*A[1][j]+c[2][k]*A[2][j];
                      }
                      break;
                   case 4 :
                      for(j=0;j<vector_len;j++)
                      {
                        cn_phi[j]=c[0][k]*A[0][j]+c[1][k]*A[1][j]+c[2][k]*A[2][j]+c[3][k]*A[3][j];
                      }
                      break;
                   case 5 :
                      for(j=0;j<vector_len;j++)
                      {
                        cn_phi[j]=c[0][k]*A[0][j]+c[1][k]*A[1][j]+c[2][k]*A[2][j]+c[3][k]*A[3][j]+c[4][k]*A[4][j];
                      }
                      break;
                   case 6 :
                      for(j=0;j<vector_len;j++)
                      {
                        cn_phi[j]=c[0][k]*A[0][j]+c[1][k]*A[1][j]+c[2][k]*A[2][j]+c[3][k]*A[3][j]+c[4][k]*A[4][j]+c[5][k]*A[5][j];
                      }
                      break;
                }

                /*set et*/
                sum = 0;
                m=36;
                for (l= 0; l < t; l++)
                {
                    et[k][l] = pow((x[m] - cn_phi[m]),2);
                    m++;
                    sum += et[k][l];
                }
                /*set error_sigma*/
                error_sigma[k] = sum;

                isig += step_size;

            }
            /*finding min error_sigma*/
            float min = error_sigma[0];
            for ( j=0 ; j < sigma_NO ; j++ )
            {
                if ( error_sigma[j] <= min )
                {
                    min = error_sigma[j];
                    sigma = j;
                }
            }

            int m1 = sigma;
            /*new sigma*/
            sigma = ((sigma-1)*step_size+lim1);

            /*set Lead_1_vector*/
            for (j=0; j<N; j++)
            {
                Lead_2_vector[j][i] = c[j][m1];
            }
            Lead_2_vector[N][i]=sigma;
        }

        /*set Hermite vector[N+1:2*(N+1)]*/
        for (j=N+1; j<2*(N+1); j++)
        {
            for (l=0; l<B_NO; l++)
            {
                Hermite_vector[j][l] = Lead_2_vector[j-(N+1)][l];
            }
        }

        for (i=0; i < N+1; i++)
        {
            free(Lead_2_vector[i]);
        }
        free(Lead_2_vector);
    }

    /*set Hermite vector[2*(N+1):(2*(N+1)+3)]*/
    for (l=0; l<B_NO; l++)
    {
        Hermite_vector[2*(N+1)][l] = R_min[l];
        Hermite_vector[(2*(N+1)+1)][l] = R_plus[l];
        Hermite_vector[(2*(N+1)+2)][l] = R_mult[l];
    }

/*------------------------normalizing---------------------------------------------*/

    float * norm = malloc (B_NO*sizeof(float));
    for (i=0; i<(2*(N+1)+3); i++)
    {
        norm = normalize(Hermite_vector[i], B_NO);
        for (j=0; j<B_NO; j++)
        {
            Hermite_vector[i][j] = norm[j];
        }
    }

    free(norm);

/*---------------------saving function result in a file----------------------------*/
    strcat(File_name, "_encoded.txt");
    FILE *fp;
    fp = fopen(strcat(PATH,File_name),"w");


        for (j=0; j<B_NO; j++)
        {
            for(i=0;i<(2*(N+1)+3);i++)
            {
                fprintf(fp, "%0.4lf \t ",Hermite_vector[i][j]);
            }
            fprintf(fp, "\n");
        }

    fclose(fp);

/*-------------------------free---------------------------------------------*/
    free(x);
    free(error_sigma);
    free(cn_phi);
    free(R_plus);
    free(R_min);
    free(R_mult);

    for (i=0; i < N; i++)
    {
        free(c[i]);
        free(A[i]);
    }
    free(c);
    free(A);

    for (i=0; i < sigma_NO; i++)
    {
        free(et[i]);
    }
  //  free(et);

    for (j = 0; j<((2*(N+1))+3); j++ )
    {
        free(Hermite_vector[j]);
    }
    free(Hermite_vector);

    for (i=0; i < vector_len; i++)
    {
        free(Beats_L1[i]);
        free(Beats_L2[i]);
    }
    free(Beats_L1);
   // free(Beats_L2);

     return 0;

}




/*normalize function*/
float * normalize(float * signal, long int len)
{
    int i;
    float * norm_sig = malloc (len*sizeof(float));

    /*find min signal*/
    float min = signal[0];
    for ( i=0 ; i < len ; i++ )
    {
        if ( signal[i] < min )
        {
            min = signal[i];
        }
    }

    /* normalizing our signal to be in the range of 0-1*/
    if (min < 0)
    {
         for ( i=0 ; i < len ; i++ )
         {
             norm_sig[i]=signal[i]+absolute(min);
         }
    }
    else
    {
        float sum=0;
        for ( i=0 ; i < len ; i++ )
         {
             norm_sig[i]=signal[i]-absolute(min);
             sum+=norm_sig[i];
         }
         if (sum == 0)
         {
             return norm_sig;
         }
    }

    /*find max norm_sig*/
    float max = norm_sig[0];
    for ( i=0 ; i < len ; i++ )
    {
        if ( norm_sig[i] > max )
        {
            max = norm_sig[i];
        }
    }

    /*linear scaling*/
    for ( i=0 ; i < len ; i++ )
    {
        norm_sig[i]=norm_sig[i]*(1/max);
    }

    return norm_sig;
}





