#include "humeds_bib.h"

void seg_delineation(char *PATH, char * File_name, int slelected_lead, float th_mult, int lead_sign, bool find_peaks, float max_RR_in, float age, float sensitivity_in, float min_RR_in, int * filters, float correction_range_in)
{
    time_t start, end;
    int i,j,k;
    FILE *fp;

    /* Signal reading */
	long int sig_len = 650000;
    float ** Leads = malloc(3 * sizeof(float*));;
    int Fs=360;
    new_read_sig(File_name, PATH, &sig_len, Leads, &Fs);

    float * sel_lead = malloc(sig_len*sizeof(float));
    memcpy(sel_lead, Leads[0],sizeof(float)*sig_len);

    fp = fopen("C:/Users/Ana/Desktop/MIT_BIH_DATA/signal.txt","w");
    for(i=0; i<sig_len; i++)
    {
        fprintf(fp, "%0.4lf \n",Leads[0][i]);
    }
    fclose(fp);

    int max_RR = (int)(Fs*max_RR_in);
    int min_RR = (int)(Fs*min_RR_in);
    int sensitivity = (int)(Fs*sensitivity_in);
    int correction_range = (int)(Fs*correction_range_in);

    long seg_size = Fs*30*60;
    long min_seg_size = Fs*5*60;
    int seg_NO;
    long lead_size = sig_len;
    int * up_limit = malloc(sizeof(int)*(floor(lead_size/seg_size)+1));
    int * down_limit = malloc(sizeof(int)*(floor(lead_size/seg_size)+1));


    i = 0;
    if(lead_size> seg_size) // more than half hour
     {
      if ((lead_size/seg_size) <1.5) //one segment
        {
         up_limit[i]=lead_size;
         down_limit[i]=0;
         seg_NO=1;
        }
        else if((lead_size/seg_size) <2) //two segments
        {
         up_limit[i]=(lead_size/2);
         down_limit[i]=0;
         i++;
         up_limit[i]=lead_size;
         down_limit[i]=lead_size-up_limit[i-1];hr_calculation
         seg_NO=2;
        }
        else
        {
            long residue = (lead_size%seg_size);
            long max_seg_NO = floor(lead_size/seg_size);
            up_limit[0]=seg_size;
            down_limit[0]=0;
            for (i=1;i<max_seg_NO-1;i++)
                {
                  up_limit[i]=(i+1)*seg_size;
                  down_limit[i]=(i+1)*seg_size-seg_size;
                }

            if (residue< min_seg_size)
               {
                 // final segment limit
                 up_limit[i] = lead_size;
                 down_limit[i] = up_limit[i-1]+1;
                 seg_NO = max_seg_NO;
               }
            else
               {
                 // final segment limit
                 up_limit[i]=up_limit[i-1]+((lead_size+residue)/2);
                 down_limit[i]=up_limit[i-1]+1;
                 i++;
                 up_limit[i]=lead_size;
                 down_limit[i]=lead_size-up_limit[i-1];
                 seg_NO = max_seg_NO+1;
               }
        }
   }
  else //one segment
   {
    up_limit[i]=lead_size;
    down_limit[i]=0;
    seg_NO=1;
   }

  int lim = Fs*2;
  long from;
  long to;
  long size;


  int z_c_1_counter_max = 2*(int)(seg_size/max_RR);
  int * z_c_1_pairs1_final = (int *)malloc(z_c_1_counter_max*sizeof(int));
  int * z_c_final = (int *)malloc(z_c_1_counter_max*sizeof(int));
  int * z_c_1_polarity_final = (int *)malloc(z_c_1_counter_max*sizeof(int));
  int * z_c_1_pairs2_final = (int *)malloc(z_c_1_counter_max*sizeof(int));
  int * morphology_final = (int *)malloc(z_c_1_counter_max*sizeof(int));
  int QRS_NO;
  int z_c_end=0,q=0;


 /** -------------------------------------- Save in File ------------------------------------------------- */
 fp = fopen("C:/Users/Ana/Desktop/MIT_BIH_DATA/zero_crossings.txt","w");

  // file size
  if (seg_NO>2)
    {
     // delineate
     to = up_limit[0]+lim;
     from = down_limit[0] ;
     size = to-from  ;
     full_delineation(sel_lead+from, size, z_c_final, z_c_1_polarity_final, z_c_1_pairs1_final, z_c_1_pairs2_final, morphology_final, &QRS_NO, Fs, th_mult, max_RR, sensitivity, min_RR, correction_range);

     for (j=0;j<QRS_NO-1; j++ )
       fprintf(fp, "%d \t %d \t %d \t %d \t %d \n",z_c_final[j], z_c_1_polarity_final[j], z_c_1_pairs1_final[j], z_c_1_pairs2_final[j], morphology_final[j]);

     //z_c(end-1)
     z_c_end = z_c_final[QRS_NO-2]+from;

     for(i=1;i<seg_NO-1;i++)
      {
       // delineate
       to = up_limit[i]+lim;
       from = down_limit[i]-lim;
       size = to-from ;
       full_delineation(sel_lead+from, size, z_c_final, z_c_1_polarity_final, z_c_1_pairs1_final, z_c_1_pairs2_final, morphology_final, &QRS_NO, Fs , th_mult, max_RR, sensitivity, min_RR, correction_range);
       //add to the matrix from z_c(end-1) +max_RR but after adding "from"
       q=0;
       while(z_c_final[q]+from<z_c_end+max_RR)
         q++;
       for (j=q;j<QRS_NO-1;j++)
       {
         //write in file z_c_final[j] and others
        fprintf(fp, "%d \t %d \t %d \t %d \t %d \n",z_c_final[j]+from, z_c_1_polarity_final[j]+from, z_c_1_pairs1_final[j]+from, z_c_1_pairs2_final[j]+from, morphology_final[j]);
       }
       z_c_end = z_c_final[QRS_NO-2]+from;

      }

     // delineate
     to = up_limit[i];
     from = down_limit[i]-lim;
     size = to-from  ;
     full_delineation(sel_lead+from, size, z_c_final, z_c_1_polarity_final, z_c_1_pairs1_final, z_c_1_pairs2_final, morphology_final, &QRS_NO, Fs , th_mult, max_RR, sensitivity, min_RR, correction_range);
     //add to the matrix from z_c(end-1) +max_RR but after adding "from"
     q=0;
     while(z_c_final[q]+from<z_c_end+max_RR)
       q++;
     for (j=q;j<QRS_NO;j++)
      {
        //write in file z_c_final[j] and others
        fprintf(fp, "%d \t %d \t %d \t %d \t %d \n",z_c_final[j]+from, z_c_1_polarity_final[j]+from, z_c_1_pairs1_final[j]+from, z_c_1_pairs2_final[j]+from, morphology_final[j]);
      }
    }
 else if (seg_NO==2)
    {
     // delineate
     to = up_limit[0]+lim;
     from = down_limit[0];
     size = to-from  ;
     full_delineation(sel_lead+from, size, z_c_final, z_c_1_polarity_final, z_c_1_pairs1_final, z_c_1_pairs2_final, morphology_final, &QRS_NO, Fs , th_mult, max_RR, sensitivity, min_RR, correction_range);
     //add to the matrix from z_c(end-1) +max_RR but after adding "from"
     for (j=0;j<QRS_NO-1; j++ )
       fprintf(fp, "%d \t %d \t %d \t %d \t %d \n",z_c_final[j], z_c_1_polarity_final[j], z_c_1_pairs1_final[j], z_c_1_pairs2_final[j], morphology_final[j]);

     z_c_end = z_c_final[QRS_NO-2]+from;

     to = up_limit[1];
     from = down_limit[1]-lim;
     size = to-from  ;
     full_delineation(sel_lead+from, size, z_c_final, z_c_1_polarity_final, z_c_1_pairs1_final, z_c_1_pairs2_final, morphology_final, &QRS_NO, Fs , th_mult, max_RR, sensitivity, min_RR, correction_range);
     //add to the matrix from z_c(end-1) +max_RR but after adding "from"
     q=0;
     while(z_c_final[q]+from<z_c_end+max_RR)
       q++;
     for (j=q;j<QRS_NO;j++)
      {
        //write in file z_c_final[j] and
        fprintf(fp, "%d \t %d \t %d \t %d \t %d \n",z_c_final[j]+from, z_c_1_polarity_final[j]+from, z_c_1_pairs1_final[j]+from, z_c_1_pairs2_final[j]+from, morphology_final[j]);
      }
    }
  else
   {
     to = up_limit[0];
     from = down_limit[0];
     size = to-from  ;
     full_delineation(sel_lead+from, size, z_c_final, z_c_1_polarity_final, z_c_1_pairs1_final, z_c_1_pairs2_final, morphology_final, &QRS_NO, Fs , th_mult, max_RR, sensitivity, min_RR, correction_range);
     for (j=0;j<QRS_NO; j++ )
       fprintf(fp, "%d \t %d \t %d \t %d \t %d \n",z_c_final[j], z_c_1_polarity_final[j], z_c_1_pairs1_final[j], z_c_1_pairs2_final[j], morphology_final[j]);

   }
  fclose(fp);


/*--------------------------------------HR calculation-------------------------------------------*/
/*  possibilities for 2nd parameter in hr function:
    1=cycle
    2=2 seconds
    10=10 seconds
    60=1 minute  */

    start=clock();


    int * RR=(float*)malloc(QRS_NO*sizeof(int));
    int * HR=(float*)malloc(QRS_NO*sizeof(int));
    hr_calculation(z_c_final,0, Fs, QRS_NO, sig_len);

    end=clock();
    printf("Execution time of HR: %f s \n", (double)(end - start)/CLOCKS_PER_SEC);


/*-----------------------------------Event detection------------------------------------------------------*/
    start=clock();

    HR_FOI(z_c_final, 10, 120, 3, 150, 3, 100, 50, 10*60, 2, Fs, sig_len, QRS_NO);

    end=clock();
    printf("Execution time of FOI: %f s \n", (double)(end - start)/CLOCKS_PER_SEC);


/*-------------------------------------------------------Hermite encoding ------------------------------------------------------------------------------*/
/*
VARIABLES:
        zero_padding_ms,QRS_len_ms are the length of the beats for the encoding
        -->
        zero_padding_ms=0.1,QRS_len_ms=0.2
        N number of hermite functions needed for the decomposition
        lim1 the low limit of sigma
        lim2 the high limit of sigma
        -->
            N=3, lim1=19.1, lim2=62;
            N=4, lim1=19, lim2=55;
            N=5, lim1=18.7,lim2=51;
            N=6, lim1=18.4, lim2=47;

        step_size is the step used for stepwise increment of sigma
        Leads_flag tells us about the used leads for clustering
        sigma_NO number of sigma
        zero_padding_len is  zero padding size on each side
        QRS_len is QRS size on each side
        vector_len is length of vector, used in hermite function
        hermites is an array of all hermites needed
*/

    start=clock();

  /*variables*/
    float zero_padding_ms=0.100;
    float QRS_len_ms=0.200;
    //Fs=360;
    int N=5;
    float lim1=18;
    float lim2=51;
    float step_size=0.5;
    int * Leads_flag = malloc (2*sizeof(int));
    Leads_flag[0] = 1;
    Leads_flag[1] = 1;
    int sigma_NO = round((lim2-lim1)/step_size);
    int zero_padding_len = 2*round(zero_padding_ms*Fs/2);
    int QRS_len = 2*round(QRS_len_ms*Fs/2);
    int vector_len=QRS_len+2*zero_padding_len;


    /*allocate hermites*/
    float ** hermites[sigma_NO];
    for (i = 0; i < sigma_NO; i ++)
    {
        hermites[i] = (float **) malloc(N *  sizeof(float));
        for (j = 0; j < N; j ++)
        {
           hermites[i][j] = (float *) malloc(vector_len *  sizeof(float));
        }
    }

/*function sets hermites[][][]*/
    Hermites(QRS_len_ms, Fs, N, lim1, lim2, step_size, sigma_NO, vector_len, hermites);

 /*function for signal encoding*/
    hermite_encoding(z_c_final,Leads[0],Leads[1],N,lim1,lim2,step_size,Leads_flag, QRS_NO,sigma_NO, zero_padding_len, QRS_len, vector_len,hermites, PATH, File_name);

     /*free hermites*/
     for (i = 0; i < sigma_NO; i ++)
        {
            for (j = 0; j < N; j ++)
            {
               free (hermites[i][j]);
            }
            free(hermites[i]);
        }
     free(hermites);

    end=clock();
    printf("Execution time of Hermite encoding: %f s \n", (double)(end - start)/CLOCKS_PER_SEC);


    free(Leads[0]);
    free(Leads[1]);
    free(Leads[2]);
    free(Leads);



}



