



#include "humeds_bib.h"
#include <math.h>


/*---------------------------------------------------------------------------
   Function :   kth_smallest()
   In       :   array of elements, # of elements in the array, rank k
   Out      :   one element
   Job      :   find the kth smallest element in the array
 ---------------------------------------------------------------------------*/

int compare (const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b );
}

int d_compare (const void *x, const void *y)
{
      register float xx = *(float*)x, yy = *(float*)y;
      if (xx < yy) return -1;
      if (xx > yy) return  1;
      return 0;

}

int float_compare (const float *x,const float *y)
{
      register float xx = *x, yy = *y;
      if (xx < yy) return -1;
      if (xx > yy) return  1;
      return 0;

}

int derv(const float arr[],float arr2[], int data_size)
{

  int i;
  for (i=1; i < data_size+1; i++)
  {
      arr2[i]=arr[i]- arr[i-1];
  }
  return 1;
}



int median(int * d, int size)
{
    int *data = malloc(sizeof(int)*size);
    memcpy(data, d, sizeof(int)*size);
    qsort(data,size, sizeof(int),compare);
    // Use data to find median
    int med = data[(int)(size/2)];
    free(data);
    return med;
}




elem_type kth_smallest(elem_type data[], int n, int k)
{
    register int  i,j,l,m ;
    register elem_type x ;

    elem_type * a= malloc(sizeof(int)*n);
    memcpy(a, data, sizeof(int)*n);

    l=0 ; m=n-1 ;
    while (l<m) {
        x=a[k] ;
        i=l ;
        j=m ;
        do {
            while (a[i]<x) i++ ;
            while (x<a[j]) j-- ;
            if (i<=j) {
                ELEM_SWAP(a[i],a[j]) ;
                i++ ; j-- ;
            }
        } while (i<=j) ;
        if (j<k) l=i ;
        if (k<i) m=j ;
    }
    return a[k] ;
}


int diff(float arr[],int data_size, float * arr2)
{

  int i;
  for (i=1; i < data_size+1; i++)
  {
      arr2[i-1]=arr[i]- arr[i-1];
  }

  return 1;
}

void remove_arr_elem(float *array, int elem_indx, int * arr_size)
{

      int  i;
      for ( i = elem_indx; i < *arr_size - 1 ; i++ )
       {
          array[i] = array[i+1];
       }

         *arr_size = *arr_size-1;

}

void remove_arr_elem_int(int *array, int elem_indx, int * arr_size)
{

      int  i;
      for ( i = elem_indx; i < *arr_size - 1 ; i++ )
       {
          array[i] = array[i+1];
       }

       *arr_size = *arr_size-1;

}


void remove_arr_elements(float *array, int upp_elem_indx, int lower_elem_indx, int * arr_size)
{

      int  i;
      int size = upp_elem_indx - lower_elem_indx;

      for ( i = lower_elem_indx ; i < *arr_size - size ; i++ )
       {
          array[i] = array[upp_elem_indx+i-1];
       }

       *arr_size = *arr_size-size-1;
}


int sign(float number)
 {
     if (number <= 0)
        return -1;
     else
        return 1;
 }


void t_d(float * arr, int size)

{
    int i;
    printf("array_printing \n");

    for (i=0; i< size; i++)

      {
          printf(" %f \n", arr[i]);
      }
      printf("Test finished \n");
}

void t_i(int * arr, int size)

{
    int i;
    printf("array_printing \n");

    for (i=0; i< size; i++)

      {
          printf(" %d \n", arr[i]);
      }
      printf("Test finished \n");
}




void thresholding_float( float * signal, float thresh, int data_size, int * new_data_size,float * mag, int *indx)
 {
     int i,j;
     j=0;
     for (i=0;i< data_size;i++ )
      {
          if ( signal[i] > thresh  )
           {
             indx[j] = i;
             mag[j]= signal[i];
             j = j+1;

           }
      }
      * new_data_size = j;

 }


void thresholding_int( int * signal, float thresh, int data_size, int * new_data_size, int mag[], int indx[])
 {
     int i,j;
     j=0;
     for (i=0;i< data_size;i++ )
      {
          if ( signal[i] < thresh  )
           {
             indx[j] = i;
             mag[j]= signal[i];
             j = j+1;
           }
      }
      *new_data_size = j;

 }


/*
void filter(int,float *,float *,int,float *,float *);
*/


void filter(int ord, float *a, float *b, int np, float *x, float *y)
{
    int i,j;
	y[0]=b[0]*x[0];
	for (i=1;i<ord+1;i++)
	{
        y[i]=0.0;
        for (j=0;j<i+1;j++)
        	y[i]=y[i]+b[j]*x[i-j];
        for (j=0;j<i;j++)
        	y[i]=y[i]-a[j+1]*y[i-j-1];
	}
    /* end of initial part */
    for (i=ord+1;i<np+1;i++)
    {
        y[i]=0.0;
            for (j=0;j<ord+1;j++)
            y[i]=y[i]+b[j]*x[i-j];
            for (j=0;j<ord;j++)
            y[i]=y[i]-a[j+1]*y[i-j-1];
    }
} /* end of filter */



float quantile(d_array arr, d_array percentages, d_array borders,int *indx_in_sorted_arr )

{

  int i;
  /* struct d_array sorted_arr;
  sorted_arr.content = (float *)malloc(sizeof(float)*(arr.len+2));
  memcpy(sorted_arr.content+1, arr.content, sizeof(float)*arr.len);
  t_d(sorted_arr.content, 25);
  qsort(sorted_arr.content+1, arr.len, sizeof(float), d_compare);
  t_d(sorted_arr.content, 25);

  sorted_arr.content[0] = sorted_arr.content[1];
  sorted_arr.content[arr.len+1] = sorted_arr.content[arr.len];
  t_d(sorted_arr.content, 25); */


  struct d_array q;
  q.len = arr.len+2;
  q.content = malloc(sizeof(float)*(arr.len+2));

  q.content[0] = 0;
  q.content[1] = 100*0.5/arr.len;
  q.content[arr.len+1] = 100;

  for(i=2; i<arr.len+1 ;i++)
    q.content[i] = (q.content[i-1] + (float)100/arr.len);

  int indx=0;
  //float elem=0;

  /* interpolation */
  for(i=0; i< percentages.len; i++)
  {

    //find(q, percentages.content[i]*100, '>','f', &indx,  &elem);
    indx = (int)(q.len*percentages.content[i]);
    indx_in_sorted_arr[i] = indx;

    if (arr.sorted_content[indx] == arr.sorted_content[indx-1] )
      borders.content[i] = arr.sorted_content[indx-1];
    else
      borders.content[i] = arr.sorted_content[indx-1] + (percentages.content[i]*100 - q.content[indx-1])*(arr.sorted_content[indx]-arr.sorted_content[indx-1])/(q.content[indx]-q.content[indx-1]) ;

  }

  float med = arr.sorted_content[(int)(arr.len/2)];
  free(q.content);
  return(med);
}


int find(d_array mat, float el, char op,char first_last, int *indx, float * eee)
{
  int i;
  *indx = -1;
  if (first_last=='f')
  {
      if (op=='>')
       for(i=0; i<mat.len ;i++)
        if (mat.content[i]>el)
           {
            *indx = i;
            *eee= mat.content[i];
            return 1;
           }
      else if(op=='=')
       for(i=0;i<mat.len ;i++)
        if (mat.content[i] == el)
           {
            *indx = i;
            *eee = mat.content[i];
            return 1;
           }
      else if(op=='<')
       for(i=0;i<mat.len ;i++)
        if (mat.content[i] < el)
           {
            *indx = i;
            *eee = mat.content[i];
            return 1;
           }
       return 0;
  }
  else
  {
       if (op=='>')
       for(i=0;i<mat.len ;i++)
        if (mat.content[i]>el)
           {
            *indx = i;
            *eee= mat.content[i];
           }
       else if(op=='=')
        for(i=0;i<mat.len ;i++)
         if (mat.content[i] == el)
            {
             *indx = i;
             *eee = mat.content[i];
            }
      else if(op=='<')
       for(i=0;i<mat.len ;i++)
        if (mat.content[i] < el)
           {
            *indx = i;
            *eee = mat.content[i];
           }
        if (*indx== -1)
         return 0;
        else
         return 1;
  }
}

/*function Convolve:
in: signal, kerenel
out:convolved signal with kernel
do: convolve kernel with signal */

void conv(float * Signal, int SignalLen, float * Kernel, int KernelLen, float * Result)
{
  register int k,n,kmin,kmax;
  register int KernelLen_1 = KernelLen - 1;


  for(n=0;n<KernelLen - 1;n++)
  {
    kmin = 0;
    kmax = n;
    for (k = kmin; k <= kmax; k++)
     {
      Result[n] += Signal[k]* Kernel[n - k];
     }
  }

 for(n=KernelLen-1;n<SignalLen-1;n++)
  {
    kmin = n -(KernelLen_1);
    kmax = n;

    for (k = kmin; k <= kmax; k++)
     {
      Result[n] += Signal[k] * Kernel[n - k];
     }
  }

 kmax = SignalLen - 1;
 for(n=SignalLen-1;n<SignalLen + KernelLen -1;n++)
  {
    kmin = n -(KernelLen_1);
    for (k = kmin; k <= kmax; k++)
     {
      Result[n] += Signal[k] * Kernel[n - k];
     }
  }

}


float threshold_computation(float *details,int len, float th_mult)
{
//    time_t  start, end;
//    float  elapsed;
    float * details_temp;
    details_temp = (float *)malloc(sizeof(float)*(len+2));
    memcpy(details_temp+1, details, sizeof(float)*len);
//    start = clock();
    qsort(details_temp+1, len, sizeof(float), d_compare);
//    end = clock();
//    elapsed = ((float) (end - start)) / CLOCKS_PER_SEC;
//    printf("%f \n", elapsed);

    details_temp[0] = details_temp[1];
    details_temp[len+1] = details_temp[len];

    //t_d(details_temp+1, 250);

    float nrm;
    struct d_array  pecentages;
    pecentages.content = malloc(sizeof(float)*5);
    pecentages.len = 5;

    pecentages.content[0] = 0.005;
    pecentages.content[1] = 0.2;
    pecentages.content[2] = 0.5;
    pecentages.content[3] = 0.75;
    pecentages.content[4] = 0.995;

    struct d_array  borders, mat;
    mat.len = len+2;
    mat.sorted_content = details_temp;
    borders.content = malloc(sizeof(float)*5);
    int *indx_in_sorted_arr;
    indx_in_sorted_arr = malloc(sizeof(int)*5);
    borders.len = 5;

//    start = clock();
    quantile( mat, pecentages, borders, indx_in_sorted_arr);
//    end = clock();
//    elapsed = ((float) (end - start)) / CLOCKS_PER_SEC;
//    printf("%f \n", elapsed);
//    start = clock();

    /* find the norm  of the selected part from the array */
    len = indx_in_sorted_arr[4]-indx_in_sorted_arr[0];
    nrm = norm(details_temp+indx_in_sorted_arr[0], len);
    float u = sqrt((float)len);

    free(details_temp);
    free(indx_in_sorted_arr);
    free(borders.content);
    free(pecentages.content);
//    end = clock();
//    elapsed = ((float) (end - start)) / CLOCKS_PER_SEC;
//    printf("%f \n", elapsed);
    return(th_mult*nrm/u);

}

float norm(float *arr, int len )
{
  int i;
  float sum_of_squares = 0.0;

  for (i=0;i<len; i++)
    {
      sum_of_squares += arr[i] * arr[i];
    }

  return sqrt(sum_of_squares);


}



void merge(int *a, int m, int *b, int n, int *sorted)
 {

  int i, j, k;

  j = k = 0;

  for (i = 0; i < m + n;) {
    if (j < m && k < n) {
      if (a[j] < b[k]) {
        sorted[i] = a[j];
        j++;
      }
      else {
        sorted[i] = b[k];
        k++;
      }
      i++;
    }
    else if (j == m) {
      for (; i < m + n;) {
        sorted[i] = b[k];
        k++;
        i++;
      }
    }
    else {
      for (; i < m + n;) {
        sorted[i] = a[j];
        j++;
        i++;
      }
    }
  }
}

int find_i(int * mat, int len, int el, char op,char first_last, int *indx, int * eee)
{
  int i;
  *indx = -1;
  if (first_last=='f')
  {
      if (op=='>')
       {
        for(i=0; i<len ;i++)
          if (mat[i]>el)
           {
            *indx = i;
            *eee= mat[i];
            return 1;
           }
       }
      else if(op=='=')
       {
        for(i=0;i<len ;i++)
         if (mat[i] == el)
           {
            *indx = i;
            *eee = mat[i];
            return 1;
           }
       }
      else if(op=='<')
       {
        for(i=0;i<len ;i++)
         if (mat[i] < el)
           {
            *indx = i;
            *eee = mat[i];
            return 1;
           }
       }
       return 0;
  }
  else
  {
       if (op=='>')
       {
       for(i=0;i<len ;i++)
        if (mat[i]>el)
           {
            *indx = i;
            *eee= mat[i];
           }
       }
       else if(op=='=')
        {
         for(i=0;i<len ;i++)
         if (mat[i] == el)
            {
             *indx = i;
             *eee = mat[i];
            }
        }
      else if(op=='<')
       {
        for(i=0;i<len ;i++)
        if (mat[i] < el)
           {
            *indx = i;
            *eee = mat[i];
           }
       }
        if (*indx== -1)
         return 0;
        else
         return 1;
  }
}


int find_a(float * mat, int len, float el, char op,char first_last, int *indx, float * eee)
{
  int i;
  *indx = -1;
  if (first_last=='f')
  {
      if (op=='>')
       {
        for(i=0; i<len ;i++)
          if (mat[i]>el)
           {
            *indx = i;
            *eee= mat[i];
            return 1;
           }
       }
      else if(op=='=')
       {
        for(i=0;i<len ;i++)
         if (mat[i] == el)
           {
            *indx = i;
            *eee = mat[i];
            return 1;
           }
       }
      else if(op=='<')
       {
        for(i=0;i<len ;i++)
         if (mat[i] < el)
           {
            *indx = i;
            *eee = mat[i];
            return 1;
           }
       }
       return 0;
  }
  else
  {
       if (op=='>')
       {
       for(i=0;i<len ;i++)
        if (mat[i]>el)
           {
            *indx = i;
            *eee= mat[i];
           }
       }
       else if(op=='=')
        {
         for(i=0;i<len ;i++)
         if (mat[i] == el)
            {
             *indx = i;
             *eee = mat[i];
            }
        }
      else if(op=='<')
       {
        for(i=0;i<len ;i++)
        if (mat[i] < el)
           {
            *indx = i;
            *eee = mat[i];
           }
       }
        if (*indx== -1)
         return 0;
        else
         return 1;
  }
}

void unique_mat(int * arr, int * len, char sorted)
{

      int size = *len;
      int  k=0;
      int temp_elem1, temp_elem2;
      int j = 0;
      int i=0;
      if (sorted =='s')
         {
          int * arr_temp =  (int *)malloc(size*sizeof(int));
          while (i <size-1)
              {
               temp_elem1= arr[i];
               j= i+1;
               temp_elem2 =arr[j];
               while(temp_elem1 == temp_elem2)
                   {
                    j=j+1;
                    temp_elem2 = arr[j];
                   }
                arr_temp[k] = temp_elem1;
                k = k+1;
                i=j;
              }
           *len = k-1;
           for(i=0;i<k;i++)
             arr[i] = arr_temp[i];
           free(arr_temp);
         }
      else
        {
         for (i = 0; i < size; i++)
          {
             for (j = i + 1; j < size;)
              {
               if (arr[j] == arr[i])
                 {
                  for (k = j; k < size; k++)
                    arr[k] = arr[k + 1];

                  size--;
                 }
                else
                  j++;
              }
            *len = size;
          }
       }
}



void delete_isolated(int * max_min,int * max_min_size, int * max_pos, int * max_pos_size, float * det, int sensitivity, int max_RR , float th_qrs)
{
        int max_min_size_temp = *max_min_size;
        int max_pos_size_temp = *max_pos_size;

        int p_s_size, weak_min_NO, weak_pos_NO,i,j;
        p_s_size = max_pos_size_temp+ max_min_size_temp;

        int * p_s;
        p_s =(int *) malloc(p_s_size*sizeof(int));

        int lim_isolated = round(sensitivity*0.7);
        int * all_weak_minima_indx = (int *) malloc(lim_isolated*sizeof(int));
        int * weak_minima_indx =(int *)malloc(lim_isolated*sizeof(int));

        merge(max_min, max_min_size_temp, max_pos, max_pos_size_temp,p_s);
        unique_mat(p_s, &p_s_size, 'n');

        int all_weak_minima_NO;


        int islolated_maxima_size = 0;
        int islolated_maxima;
        int indx;
        int eee;
        for(i=0;i<p_s_size-1;i++)
          {
              if ((p_s[i+1]-p_s[i]> max_RR*1.5) & (p_s[i+2]-p_s[i+1]> max_RR*1.5))
                    {
                        islolated_maxima = p_s[i+1];
                        islolated_maxima_size = islolated_maxima_size+1;

                        // Is it strong maxima?
                        if (det[islolated_maxima]>th_qrs*1.5)
                          {

                            weak_min_NO = peak_finder(det+islolated_maxima-lim_isolated, 2*lim_isolated, 0.0001, th_qrs/1.5, all_weak_minima_indx, weak_minima_indx, &all_weak_minima_NO );
                            if (weak_min_NO == 0)
                            {
                              // delete isolated maxima
                              find_i(max_pos, max_pos_size_temp, islolated_maxima, '=', 'f', &indx, &eee);
                              remove_arr_elem_int(max_pos, indx, &max_pos_size_temp);

                            }
                            else
                            {
                              for (j=0;j<weak_min_NO;j++)
                                weak_minima_indx[j] = weak_minima_indx[j]+islolated_maxima-lim_isolated;
                              // Add the minima to the max_2_min
                              append(max_pos, &max_pos_size_temp , weak_minima_indx , &weak_min_NO);

                            }
                          }
                          // Is it strong minima?
                          else if(det[islolated_maxima]<(-th_qrs*1.5))
                          {
                            //weak_pos_NO = peak_finder
                            weak_pos_NO = peak_finder(det+islolated_maxima-lim_isolated, 2*lim_isolated, 0.0001, th_qrs/1.5, all_weak_minima_indx, weak_minima_indx, &all_weak_minima_NO );

                            if (weak_pos_NO == 0)
                            {
                             // delete isolated minima
                              find_i(max_min, max_min_size_temp, islolated_maxima, '=', 'f', &indx, &eee);
                              remove_arr_elem_int(max_min, indx, &max_min_size_temp);
                            }
                            else
                            {
                             for (j=0;j<weak_pos_NO;j++)
                                weak_minima_indx[j] = weak_minima_indx[j]+islolated_maxima-lim_isolated;

                             // delete the maxima to max_2_pos
                             /* Appending */
                             append(max_min, &max_min_size_temp , weak_minima_indx , &weak_pos_NO);

                            }

                          }
                          else if(det[islolated_maxima]<0)
                          {
                              // delete isolated minima
                              find_i(max_min, max_min_size_temp, islolated_maxima, '=', 'f', &indx, &eee);
                              remove_arr_elem_int(max_min, indx, &max_min_size_temp);
                          }
                          else if(det[islolated_maxima]>0)
                          {
                              // delete isolated minima
                              find_i(max_pos, max_pos_size_temp, islolated_maxima, '=', 'f', &indx, &eee);
                              remove_arr_elem_int(max_pos, indx, &max_pos_size_temp);
                          }
                    }
          }

        free(p_s);
        free(all_weak_minima_indx);
        free(weak_minima_indx);

        *max_pos_size = max_pos_size_temp;
        *max_min_size = max_min_size_temp;
        /* ------------------------------------------------------------------------------------------- */

}


int append(int *arr, int * arr_len, int *arr2,int * arr2_len)
{
  int i=0;
  for (i=0;i<*arr2_len;i++)
   {
     arr[*arr_len+i]=arr2[i];
   }
   *arr2_len = *arr_len+ *arr2_len;
  return(1);
}



int zero_crossing(float * arr, int arr_size, char first_last)
{
    int i;
    if (arr_size<2)
      return (-1);
    else if (first_last=='f')
      {
          i = 0;
          while ((sign(arr[i])==sign(arr[i+1])) && ((i+1) <= arr_size))
            i = i+1;
          if (i<arr_size)
            return(i);
      }
    else
     {
         i = arr_size-1;
         while ((sign(arr[i])==sign(arr[i-1])) && ((i-1) >= 0))
            i = i-1;
          if (i>0)
            return(i);
     }
     return(-1);

}

void max_mat(float * mat, int mat_size, int * indx, float * max_val)
{
  register float maxim=-99999999;
  register int i;
  for(i=0;i<mat_size;i++)
  {
   if(mat[i]>maxim)
      {maxim = mat[i]; *indx=i;}
  }
  *max_val = maxim;
}
void min_mat(float * mat, int mat_size, int * indx, float * min_val)
{
  register float minim=+99999999;
  register int i;
  for(i=0;i<mat_size;i++)
  {
   if(mat[i]<minim)
      {minim = mat[i]; *indx=i;}
  }
  *min_val = minim;
}

void delete_near_peaks(int * arr, int * arr_size, int distance, float * sig,bool min_max)

{
  int i;
  if (min_max)
      for (i=0;i<*arr_size-1;i++)
        {
            if ((arr[i+1] -arr[i])<distance)
            {
                if(sig[arr[i+1]] < sig[arr[i]])
                   remove_arr_elem_int(arr, i, arr_size);
                 else
                   remove_arr_elem_int(arr, i+1, arr_size);

            }
        }
  else
     for (i=0;i<*arr_size-1;i++)
        {
            if ((arr[i+1] -arr[i])<distance)
            {
                if(sig[arr[i+1]] > sig[arr[i]])
                   remove_arr_elem_int(arr, i, arr_size);
                 else
                   remove_arr_elem_int(arr, i+1, arr_size);

            }
        }

}



/**
function to insert array in another one good
memory reallocation should be done before this
**/
int insert_in_array(int *arr1,int arr1_len,int * arr2, int arr2_len, int indx)
{
   int i;
   for(i=0 ; i<arr1_len-indx+1;i++)
       arr1[arr1_len-1-i+arr2_len] = arr1[arr1_len-1-i];

   for (i =0; i<arr2_len;i++)
        arr1[indx+i] =  arr2[i];
   return(1);
}



void correction_to_maxima(float * signal, int indx_before, int * indx_after, int range, int polarity)
{
  int i;
  float min,max;
  int indx_before_temp = indx_before;

  if (polarity==-1)
  {
     min = signal[indx_before_temp];
     *indx_after = indx_before_temp;
    for(i = indx_before_temp-range; i<indx_before_temp+range;i++)
      {
       if (signal[i]<min)
          {
            *indx_after = i;
             min=signal[i];
          }
      }
  }
  else
  {
    max = signal[indx_before_temp];
    *indx_after = indx_before_temp;
    for(i = indx_before_temp-range; i<indx_before_temp+range;i++)
      {
       if (signal[i]>max)
         {
           *indx_after = i;
           max=signal[i];
         }
      }
  }
}


int  find_peaks_levels(int *peaks_Indx_up_level, int peaks_NO_up_level, int *peaks_Indx_lower_level, int peaks_NO_lower_level, int lim, float * det, int * max_pos )

{

         float N_P_mag, peak_mag;

         int p, N_P_ind, j, peak_Indx, k = 0; //N_P_ind nearest peak index
         int i,i2;
//         int  i1, distance;
         int * max_temp =(int *) malloc(lim*sizeof(int));



         for(i=0; i< peaks_NO_up_level;i++)
          {
            p = peaks_Indx_up_level[i];
            j = 0;

            optimized_search_for_pairs(peaks_Indx_lower_level, peaks_NO_lower_level, p, lim, max_temp,&j, &N_P_ind);

            /*
            for (i1=0; i1<peaks_NO_lower_level; i1++)
              {
               distance = abs(peaks_Indx_lower_level[i1]-p);
               if(distance<=lim)
                 {
                  max_temp[j] = peaks_Indx_lower_level[i1];
                  if (distance < abs(N_P_ind-p) )
                     N_P_ind = peaks_Indx_lower_level[i1];
                  j++;
                 }
              }
              */

            if(j<=0)
              continue;

            N_P_mag = det[N_P_ind];
            peak_mag = N_P_mag;
            peak_Indx = N_P_ind;
            for (i2=0 ; i2<j; i2++)
              {
                 if(det[max_temp[i2]]/N_P_mag>=1.2)
                   {
                    if (det[max_temp[i2]]>peak_mag)
                      {
                        peak_mag = det[max_temp[i2]];
                        peak_Indx =  max_temp[i2];
                      }
                   }
              }

            max_pos[k] = peak_Indx;
            k = k+1;
          }
         free(max_temp);
         return (k);

}


int  find_valleys_levels(int *peaks_Indx_up_level, int peaks_NO_up_level, int *peaks_Indx_lower_level, int peaks_NO_lower_level,int lim,float * det, int * max_pos )

{

         float N_P_mag, peak_mag;
         int p, N_P_ind, j, peak_Indx, k = 0; //N_P_ind nearest peak index
         int ii,i2;
//         int i1, distance;
         int * max_temp;
         max_temp = malloc(lim*sizeof(int));

         for(ii=0; ii< peaks_NO_up_level;ii++)
          {
            p = peaks_Indx_up_level[ii];
            j = 0;
            N_P_ind = lim;
            optimized_search_for_pairs(peaks_Indx_lower_level, peaks_NO_lower_level, p, lim, max_temp,&j, &N_P_ind);
            /*
            for (i1=0; i1<peaks_NO_lower_level; i1++)
              {
               distance = abs(peaks_Indx_lower_level[i1]-p);
               if(distance<=lim)
                 {
                  max_temp[j] = peaks_Indx_lower_level[i1];
                  if (distance < abs(N_P_ind-p) )
                     N_P_ind = peaks_Indx_lower_level[i1];
                  j++;
                 }
              }
            */
            if(j<=0)
              continue;

            N_P_mag = det[N_P_ind];
            peak_mag = N_P_mag;
            peak_Indx = N_P_ind;
            for (i2=0 ; i2<j; i2++)
              {
                 if(det[max_temp[i2]]/N_P_mag>=1.2)
                   {
                    if (det[max_temp[i2]]<peak_mag)
                      {
                        peak_mag = det[max_temp[i2]];
                        peak_Indx =  max_temp[i2];
                      }
                   }
              }

            max_pos[k] = peak_Indx;
            k = k+1;
          }
         free(max_temp);
         return (k);

}



void preprocessing(d_array signal, int *filters, float lead_gain,int lead_sign, int Fs)
 {
   int i;
   lead_gain = lead_gain* lead_sign;

   if(lead_gain != 1)
       for (i=0;i<signal.len;i++)
         signal.content[i] = signal.content[i] *(lead_gain);

   if (filters[0]==1) // Notch filtering
         {
            float *notched_signal;
            notched_signal=(float *) malloc((signal.len)*sizeof(float));
            notch_filtering(signal.content, signal.len, notched_signal, Fs);
            signal.content = notched_signal;
         }

   if (filters[1] == 1) // Artifact removal filtering
         {
          //function to find the big jumps
          //function to threshold the big jumps
          //function to threshold using quantiles
         }
   if (filters[2] == 1) // Adaptive Denoising
     {

     }

   if (filters[3] == 1) // Adaptive Denoising
     {

     }

   if (filters[4]==1)
    {
      // High pass filtering

    }

 }


void calibration(float *signal, float lead_off_RR, float lead_off_amplitude, float lead_off_value)
{




}

void reverse_mult( float  signal[], int sig_len, float factor)

{
  int i;
  for (i=0;i< sig_len; i++)
    {
      signal[i] = factor*signal[i];
    }

}


float absolute(float number)
{
    if (number < 0)
        return -number;
    else
        return number;
}

float abs_mat(float * arr, int arr_len)
{
    int i;
    for(i=0;i<arr_len;i++)
    {
        if (arr[i] < 0)
            arr[i] = -arr[i];
        else
            arr[i] = arr[i];
    }
}

float mean_float (float * array, long int arr_size)
{
    int i;
    float sum=0;
    if (arr_size == 0)
      return (0);

    for (i=0; i<arr_size; i++)
    {
        sum += array[i];
    }

    return (sum/arr_size);
}



double gaussrand() // http://c-faq.com/lib/gaussian.html
{
	static double V1, V2, S;
	static int phase = 0;
	double X;

	if(phase == 0) {
		do {
			double U1 = (double)rand() / RAND_MAX;
			double U2 = (double)rand() / RAND_MAX;

			V1 = 2 * U1 - 1;
			V2 = 2 * U2 - 1;
			S = V1 * V1 + V2 * V2;
			} while(S >= 1 || S == 0);

		X = V1 * sqrt(-2 * log(S) / S);
	} else
		X = V2 * sqrt(-2 * log(S) / S);

	phase = 1 - phase;

	return X;
}

void awgn(float *  arr, int arr_size, int SNR,  float * noised_arr)
{
	/*
		Adds white Gaussian noise to signal, signal power is assumed to be 0 dBW (this is taken from Matlab)
		- SNR is in dB
	*/
	int i;
	float sigPower, noisePower;
	sigPower = 0;
	for(i = 0; i<arr_size; i++){
		sigPower = sigPower+arr[i]*arr[i];
	}
	sigPower = sigPower/arr_size;
	sigPower = 10*log10(sigPower); // This is bug, if signal is zero this should be fixed...
	noisePower = sigPower-SNR;
	noisePower = pow(10,noisePower/10);
	for(i = 0; i<arr_size; i++){
		noised_arr[i] = arr[i]+sqrt(1*noisePower)*gaussrand();
	}
}

/*RR intervals*/
int rr(int R_points[],int data_size, float * RR)
{
    int i;

    for (i=1; i < data_size; i++)
    {
        RR[i] = R_points[i] - R_points[i-1];
    }
    RR[0] = RR[1];
    return 1;
}


/*factorial function*/
int factorial(int n)
{
    if (n<=1)
        return(1);
    else
        n=n*factorial(n-1);
    return(n);
}

/*remove duplicate*/
void unique(int * arr, int size, int*new_size)
{
    int elt1, elt2;
    int k=0;
    int i=0;
    int j;
    int * arr_temp =  (int *)malloc(size*sizeof(int));
    while (i <size-1)
    {
        elt1= arr[i];
        j= i+1;
        elt2 =arr[j];
        while(elt1 == elt2)
        {
            j++;
            elt2 = arr[j];
        }
        arr_temp[k] = elt1;
        k++;
        i=j;
    }
    for(i=0;i<k;i++)
    {
        arr[i] = arr_temp[i];
    }
    *new_size=k;
    for(i=k;i<size;i++)
    {
        arr[i]=0;
    }
    free(arr_temp);
}

