
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <dirent.h>

#ifndef HUMEDS_BIB_H_INCLUDED
#define HUMEDS_BIB_H_INCLUDED





typedef struct d_array
{
  int len;
  float * content;
  float * sorted_content;
  float * mean;
  float * med;
} d_array;

typedef int elem_type ;

typedef enum { false, true } bool;

/* Macros */
#define median2(a,n) kth_smallest(a,n,(((n)&1)?((n)/2):(((n)/2)-1)))

#define ELEM_SWAP(a,b) { register elem_type t=(a);(a)=(b);(b)=t; }

#ifndef max
    #define max(a,b) ((a) < (b) ? (a) : (b))
#endif

#ifndef min
    #define min(a,b) ((a) > (b) ? (a) : (b))
#endif


/* Functions */

elem_type kth_smallest(elem_type a[], int n, int k);

int zero_crossing(float * arr, int elem, char first_last);

void notch_filtering(float *data, int data_size, float *c_signal, int fs);

void  wavelet(float *signal,int level_NO,int details_flag,int data_size,float * app[], float * details[]);

int diff(float arr[],int data_size, float * arr2);

void remove_arr_elem(float *array, int elem_indx, int * arr_size);

void remove_arr_elem_int(int *array, int elem_indx, int * arr_size);

void unique_mat(int * arr, int * len, char sorted);

void remove_arr_elements(float *array, int upp_elem_indx, int lower_elem_indx, int * arr_size);

int find(d_array mat, float el, char op,char first_last, int *indx, float * eee);

int find_a(float * mat, int len, float el, char op,char first_last, int *indx, float * eee);

int sign(float number);

void t_d(float * arr, int size);

int median(int * d, int size);

void   find_R(float * details[], float* signals, int point1, int point2, float th_mult, int correction_range,
              int Fs, int sensitivity, int min_RR, int max_RR, bool slow_HR, int * peaks_l4, int * peaks_l3, int * peaks_l2,
              int * peaks_l1, int * peaks_NO, int *valleys_l4, int *valleys_l3,int * valleys_l2, int * valleys_l1,
              int * valleys_NO, int * z_c_1_arr, int * z_c_1_pairs1, int * z_c_1_pairs2 , int * z_c_1_polarity,
              int * new_peaks_NO);

void t_i(int * arr, int size);

int derv(const float arr[],float arr2[], int data_size);

int new_read_sig(char * file_name, char * path,  long int * sig_len, float * M[], int * Fs);

int hermite_encoding(int * R_peaks_indx, float * Lead1, float * Lead2,int N,float lim1,float lim2, float step_size,int * Leads_flag, long int length, int sigma_NO,int zero_padding_len, int QRS_len, int vector_len, float *** hermites, char *PATH, char * File_name);

int Hermites(float QRS_len_ms, int Fs, int N,float lim1,float lim2, float step_size, int sigma_NO, int vector_len, float *** hermites);

float * Hermite_fun(float * t,int len,float sigma,int n);

float * hermite(int n,float * x, float sigma,int len);

float * normalize(float * signal, long int len);

int hr(int * z_c_final, int time, int Fs, int QRS_NO, int sig_len);

int HR_FOI(int * R_points, int sigma, int VTR, int VTL, int SVTR, int SVTL, int TR, int BR, int interval_len, int card_pause, int Fs, int sig_len, int QRS_NO);

float absolute(float number);

void thresholding_float( float * signal, float thresh, int data_size, int * new_data_size,float * mag, int *indx);

float threshold_computation(float *details,int len, float th_mult);

void conv(float * Signal, int SignalLen, float * Kernel, int KernelLen, float * Result);

void thresholding_int( int * signal, float thresh, int data_size, int * new_data_size, int mag[], int indx[]);

void filter(int ord, float *a, float *b, int np, float *x, float *y);

int compare (const void * a, const void * b);

int d_compare (const void *x, const void *y);

float quantile(d_array arr, d_array percentages, d_array borders,int *indx_in_sorted_arr );

int Single_lead_delineate(float * signals, int sig_len, float th_mult, int lead_sign, bool find_peaks, int Fs, int max_RR, int sensitivity,
                          int min_RR, int * filters,int correction_range, float * th_qrs, float * det[], int * QRS_NO, int * z_c_final, int * z_c_1_pairs1_final,
                          int * z_c_1_pairs2_final, int * z_c_1_polarity_final);

void polarity_validation(int * z_s_1,int QRS_NO, int Fs, int * z_s_1_polarity, int *associated_pairs_1,int *associated_pairs_2, float * details_2, float th_qrs_2, int * morphology);

void preprocessing(d_array signal, int *filters, float lead_gain,int lead_sign, int Fs);

void max_mat(float * mat, int mat_size, int * indx, float * max_val);

void min_mat(float * mat, int mat_size, int * indx, float * min_val);

void merge(int * a, int m, int * b, int n, int * sorted);

int append(int *arr, int * arr_len, int *arr2,int * arr2_len);

float norm(float * arr, int len );

int find_bin(int * array, int arr_size, int key, char op, char firs_last);

int  peak_finder(float * signal, int data_size, float sel, float thresh, int * All_peaks_Inds, int * peaks_Indx,int * all_peaks_NO);

int find_i(int * mat, int len, int el, char op,char first_last, int *indx, int * eee);

void delete_isolated(int * max_min,int * max_min_size, int * max_pos, int * max_pos_size, float * det, int sensitivity, int max_RR , float th_qrs);

void delete_near_peaks(int * arr, int * arr_size, int distance, float * sig,bool min_max);

void reverse_mult( float  signal[], int sig_len, float factor);

void calibration(float *signal, float lead_off_RR, float lead_off_amplitude, float lead_off_value);

void preprocessing(d_array signal, int *filters, float lead_gain,int lead_sign, int Fs);

int  find_valleys_levels(int *peaks_Indx_up_level, int peaks_NO_up_level, int *peaks_Indx_lower_level, int peaks_NO_lower_level,int lim,float * det, int * max_pos );

int  find_peaks_levels(int *peaks_Indx_up_level, int peaks_NO_up_level, int *peaks_Indx_lower_level, int peaks_NO_lower_level,int lim,float * det, int * max_pos );

void seg_delineation(char *PATH, char * File_name, int slelected_lead, float th_mult, int lead_sign, bool find_peaks, float max_RR_in, float age, float sensitivity, float min_RR_in, int * filters, float correction_range);

void correction_to_maxima(float * signal, int indx_before, int * indx_after, int range, int polarity);

int insert_in_array(int *arr1,int arr1_len,int * arr2, int arr2_len, int indx);

void Polarity_correction(int * z_s_1,int *z_s_1_polarity, int *associated_pairs_1,float *details_2,float *details_1, int sensitivity, float th_qrs_2, int QRS_NO);

void delete_near_peaks(int * arr, int * arr_size, int distance, float * sig,bool min_max);

void find_double_R(int * morphology, int * z_s_1,float * details_2, int Fs, float th_qrs_2, int *associated_pairs_1,int QRS_NO);

int  remove_baseline(char method[], int win_L1, int win_L2, int *data, int data_size, int * baseline);

int median(int * d, int size);

int float_compare (const float *x,const float *y);

int optimized_search_for_pairs(int * max_min, int arr_size, int key, int lim, int * max_temp, int* k, int *N_P_ind);

int optimized_search_for_zc(int * arr, int arr_size, int key, int lim, int *N_P_indx, int *indx_of_nearest);

void full_delineation(float * leads_d, int sig_len, int * z_c_final, int * z_c_1_polarity_final, int * z_c_1_pairs1_final,
                      int * z_c_1_pairs2_final, int * morphology_final, int * QRS_Number, int Fs, float th_mult, int max_RR,
                      int sensitivity, int min_RR, int correction_range);

#endif // HUMEDS_BIB_H_INCLUDED

