#include "humeds_bib.h"

int hr(int * z_c_final, int interval_time, int Fs, int QRS_NO, int sig_len)
{
    int i,j,k;
    FILE* fp;
    fp = fopen("C:/Users/Ana/Desktop/MIT_BIH_DATA/HR.txt","w");

    /*RR*/
    float * RR=(int*)malloc(QRS_NO*sizeof(int));
    rr(z_c_final,QRS_NO,RR);

    /*variables*/
    int QRS,a,b;
    int before;
    int after;
    int HR=0;
    int d=0;
    int from, to;
    int sum;
    int begin,end;

/*-------------------------------------cycles-----------------------------------------*/
    if(interval_time==1)
    {
        /*calculate HR*/
        HR=(RR[1]+RR[0])/2;
        HR=60*Fs/HR;
        /*print results in .txt */
        fprintf(fp, "%d \n", HR);
        for (i=1;i<QRS_NO;i++)
        {
            HR=(RR[i]+RR[i-1])/2;
            HR=60*Fs/HR;
            /*print results in .txt */
            fprintf(fp, "%d \n", HR);
        }
    }

    if(interval_time==0)
    {
       /*calculate HR*/
        for (i=0;i<QRS_NO;i++)
        {
            HR=60*Fs/RR[i];
            /*print results in .txt */
            fprintf(fp, "%d \n", HR);
        }
    }

/*-----------------------------------------2 sec, 10 sec or 1 minute-----------------------*/
    else
    {
        /*set intervals*/
        int interval=Fs*interval_time;
        int new_interval=Fs*(interval_time/2);

        /*calculate HR*/
        /*find min QRS*/
        for(i=1;i<QRS_NO;i++){
            if((z_c_final[i]-new_interval)>0){
                begin=i;
                break;
            }
            else{begin=1;}
        }

        /*find max QRS*/
        for(i=begin;i<QRS_NO;i++){
            if((z_c_final[i]+new_interval)>sig_len){
                end=i-1;
                break;
            }
            else{end=QRS_NO;}
        }

    /*--------------------------from 0 to begin- count only QRS after--------------------------------------*/
        for (i=0;i<begin;i++)
        {
            QRS=z_c_final[i];
            b=QRS+interval;
            // b=QRS+new_interval;
            /*QRS after*/
            sum=0;
            HR=0;
            for(j=i;j<QRS_NO;j++){
                sum+=RR[j];
                d=QRS+sum;
                if(d<=b){
                    HR++;
                    to=j+1;
                }
                else{
                    to=j;
                    break;
                }
            }
            /*average RR*/
             /*if there are no QRS complexes after*/
            if(to==i)
            {
                HR=RR[i];
            }
             /*otherwise*/
            else
            {
                sum=0;
                for(k=i;k<to;k++)
                {
                    sum+=RR[k];
                }
                HR=sum/HR;
            }
                HR=60*Fs/HR;

            /*print results in .txt */
            fprintf(fp, "%d \t %d \t %s %d \n",QRS, b,"HR: ", HR);
         }


    /*--------------------------------from begin to end----------------------------------------------*/
        for (i=begin;i<end;i++){
            QRS=z_c_final[i];
            a=QRS-new_interval;
            b=QRS+new_interval;
            sum=0;
            before=0;
            /*QRS before*/
            for(j=i-1;j>=0;j--){
                sum+=RR[j];
                d=QRS-sum;
                if(d>a){
                    before++;
                    from=j;
                }
                else{
                    from=j+1;
                    break;
                }
            }
            /*QRS after*/
            sum=0;
            after=0;
            for(j=i;j<QRS_NO;j++){
                sum+=RR[j];
                d=QRS+sum;
                if(d<=b){
                    after++;
                    to=j+1;
                }
                else{
                    to=j;
                    break;
                }
            }
            HR=(before+after);
            /*average RR*/
            /*if there are no QRS complexes before and after*/
            if(from==to)
            {
                HR=RR[i];
            }
            /*otherwise*/
            else
            {
                sum=0;
                for(k=from;k<to;k++)
                {
                    sum+=RR[k];
                }
                HR=sum/HR;
            }
            HR=60*Fs/HR;

            /*print results in .txt */
            fprintf(fp, "%d \t %d \t %s %d \n",a, b,"HR: ", HR);
        }

    /*----------------------from end to QRS_NO - only QRS before---------------------------------*/
    /*----------------------if end==QRS --> this will not be executed! -----------------------------------------------*/
        for (i=end;i<QRS_NO;i++)
        {
            QRS=z_c_final[i];
            a=QRS-interval;
            //a=QRS-new_interval;
            sum=0;
            HR=0;
            /*QRS before*/
            for(j=i-1;j>=0;j--){
                sum+=RR[j];
                d=QRS-sum;
                if(d>a){
                    HR++;
                    from=j;
                }
                else{
                    from=j+1;
                    break;
                }
            }
            /*average RR*/
             /*if there are no QRS complexes before*/
            if(from==i)
            {
                HR=RR[i];
            }
             /*otherwise*/
            else
            {
                sum=0;
                for(k=from;k<i;k++)
                {
                    sum+=RR[k];
                }
                HR=sum/HR;
            }
                HR=60*Fs/HR;

            /*print results in .txt */
            fprintf(fp, "%d \t %d \t %s %d \n",a, QRS,"HR: ", HR);
         }
    }

/*----------------------------------------------------------------------------------------------------*/
    fclose(fp);
    free(RR);

    return 0;
}

