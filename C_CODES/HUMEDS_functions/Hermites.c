 #include "humeds_bib.h"

/**----------------------------------------------------------------------------------------------
HERMITES:
Description:
    function calculates hermite functions and sets them in hermites[][][]
Inputs: QRS_len_ms is the length of the beats for the encoding
        Fs is sampling frequency
        N number of hermite functions needed for the decomposition
        lim1 the low limit of sigma
        lim2 the high limit of sigma
        step_size is the step used for stepwise increment of sigma
        Sigma_NO is number of sigma
        vector_len is length of vector, used in hermite function
        hermites is an array of all hermites needed
Output: hermites[][][]


Example: Hermites(0.200, 360, 5, 18, 51, 0.5, sigma_NO, vector_len, hermites);




-----------------------------------------------------------------------------------------------**/

 int Hermites(float QRS_len_ms, int Fs, int N, float lim1,float lim2, float step_size,int sigma_NO, int vector_len, float *** hermites)
{
 /*variables*/
    int i, j, k;

 /*allocate and set vector*/
    float * vector = (float*) malloc (vector_len * sizeof(float));
    float start = (-QRS_len_ms + (1.0/Fs/2.0))*1000.0;
    vector[0] = start;
    float step = (1.0/Fs)*1000;
    for (i=1; i < vector_len; i++)
    {
        vector[i] = vector[i-1] + step;
    }

    /*Making a an array of all hermites needed*/
    /*allocate hermites*/


    /*allocate y - tmp array for setting hermites*/
    float * y = malloc(vector_len*sizeof(float));

    /*set hermites*/
    int sigma;
    float sig;
    for (sigma = 0; sigma < sigma_NO; sigma ++)
    {
        sig = lim1 + step_size *sigma;
        for (j=0; j < N; j++)
        {
            y = Hermite_fun(vector,vector_len,sig,j);
            for(k=0; k < vector_len; k++)
            {
                hermites[sigma][j][k] = y[k];
            }
        }

    }

    return 0;
}


/*--------------functions used in function Hermites---------------*/


/*hermite_fun function*/
float * Hermite_fun(float * t,int len,float sigma,int n)
{
        float * f =  malloc (len*sizeof(float));
        float * y =  malloc (len*sizeof(float));
        double pi = sqrt(3.1416);
        int i;

        /*call hermite function*/
        f = hermite(n, t, sigma, len);

        float t1= 1/sqrt(sigma*pow(2,n)*factorial(n)*pi);
        float t2= 2*pow(sigma,2);
        for(i=0; i < len; i++)
        {
            float t3 = - pow(t[i],2);
            float t4=exp(t3/t2);
            y[i]=t1 * t4 * f[i];
        }

        free(f);
        return y;
}


/*recursive hermite function*/
int * hermite_rec(int n)
{
    int * h =  malloc ((n+1)*sizeof(int));
    int * h1 = malloc ((n+1)*sizeof(int));
    int * h2 = malloc ((n+1)*sizeof(int));
    int * h1tmp = malloc ((n+1)*sizeof(int));
    int * h2tmp = malloc ((n+1)*sizeof(int));
    int i;

    if( n == 0 )
    {

        h[0] = 1;
    }
    else if( n == 1 )
    {
        h[0] = 2;
        h[1] = 0;
    }
    else
    {

        h1tmp = hermite_rec(n-1);
        h2tmp = hermite_rec(n-2);

        /*h1*/
        h1[n]= 0;
        for (i=0; i < n; i++)
        {
            h1[i]= 2*h1tmp[i];
        }


        /*h2*/
        for (i=2; i < (n+1); i++)
        {
            h2[i]=  2*(n-1)* h2tmp[i-2];
        }
        for (i=0; i < 2; i++)
        {
            h2[i]= 0;
        }


        /*h*/
        for (i=0; i < n+1; i++)
        {
            h[i] = h1[i] - h2[i];
        }
    }
    free(h1);
    free(h2);

    return h;
}

/*hermite function*/
float * hermite(int n,float * x, float sigma, int len)
{
    int * g = malloc (n+1*sizeof(int));
    float * w = malloc (len*sizeof(float));
    int i, j, p;

    /* call the hermite recursive function*/
    g = hermite_rec(n);

    /*evaluate the hermite polynomial function, given x*/
    for (i=0;i < len; i++)
    {
        w[i] = g[n];
        p = 1;
        for (j=n-1;j >= 0; j--)
        {
            w[i] = w[i] + g[j] * pow((x[i]/sigma),p);
            p++;

        }
    }
    free(g);
    return w;

}
